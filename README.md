# Franken Semantic UI

Franken Semantic UI CSS step by step, from pure html+css, to less.
Using custom spacing helper, color from materialize CSS,
and a little bit Bulma.

## Custom

This is a custom CSS with a few Semantic UI,
rather than Semantic UI with a few custom CSS.

-- -- --

## Links

### Franken Semantic UI

This repository:

* [Semantic UI Step by Step Repository][tutorial-semantic-ui]

### Comparation

Comparation with other guidance:

* [Bulma Step by Step Repository][tutorial-bulma]

* [Materialize Step by Step Repository][tutorial-materialize]

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/bulma-material-design/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma/
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

-- -- --

## Tutor 01

> Adding Semantic UI CSS

* Without Semantic UI CSS

* Using CDN

* Using Local

![Tutor 01][screenshot-01]

## Tutor 02

> Using Semantic UI: Navigation Bar

* Simple

* Full Featured (with jQuery)

* Using Icon (compatible with FontAwesome)

![Tutor 02][screenshot-02]

## Tutor 03

> Custom Less

* Custom maximum width class

* Responsive using Semantic UI's

![Tutor 03][screenshot-03]

## Tutor 04

> Helper Class

* Custom Less Spacing Helper 

* Custom Less Color Helper (Ported from Materialize CSS)

![Tutor 04][screenshot-04]

## Tutor 05

> HTML Box using Semantic UI

* Main Blog and Aside Widget

![Tutor 05][screenshot-05]

## Tutor 06

> Finishing

* Blog Post Example

![Tutor 06][screenshot-06]

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot-01]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-01/html-semantic-ui-preview.png
[screenshot-02]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-02/html-semantic-ui-preview.png
[screenshot-03]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-03/html-semantic-ui-preview.png
[screenshot-04]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-04/html-semantic-ui-preview.png
[screenshot-05]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-05/html-semantic-ui-preview.png
[screenshot-06]:         https://gitlab.com/epsi-rns/tutor-html-semantic-ui//raw/master/step-06/html-semantic-ui-preview.png
