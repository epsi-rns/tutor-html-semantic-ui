'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');


gulp.task('less', function () {
  return gulp.src(['./less/main.less'], './less/helper.less')
    .pipe(less({ 
       compress: true, 
       paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./css'));
});

gulp.task('default', gulp.series('less'));
